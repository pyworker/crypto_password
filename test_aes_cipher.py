import unittest
from os import remove
from os.path import exists
# from unittest.mock import patch
from aes_cipher import AESCipher
from secret_key import SecretKey
from hashlib import md5

TEST_KEY_PATH = '.test_key'
TEST_HASH_PATH = '.test_hash'
RAW = 'secret_phrase'


class TestAESCipher(unittest.TestCase):

    def test_encrypt_decrypt(self):
        """
        currect encrypt and decrypt
        """
        aes = AESCipher()
        k = SecretKey()
        # prepare env before test 
        for item in [TEST_KEY_PATH, TEST_HASH_PATH, RAW]:
            try:
                remove(item)
                print('removed %s' % item)
            except OSError:
                pass

        should_ans = RAW
        key = k.generate_key(password=RAW, fname=TEST_HASH_PATH)
        encrypt_msg = aes.encrypt_to_file(source=RAW, key=key, fname=TEST_KEY_PATH)
        key = k.get(fname=TEST_HASH_PATH)
        ans = aes.decrypt_from_file(fname=TEST_KEY_PATH, key=key)
        self.assertEqual(ans, should_ans)

        # clean env after test 
        for item in [TEST_KEY_PATH, TEST_HASH_PATH, RAW]:
            try:
                remove(item)
                print('removed %s' % item)
            except OSError:
                pass


if __name__ == '__main__':
    unittest.main()