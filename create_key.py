#!/usr/bin/env python2.7

"""
module for creating a secret key
"""


from getpass import getpass, getuser
from sys import exit
from os import remove
from os.path import exists
from secret_key import SecretKey

HASH_PATH = '.hash'

def main():

    if exists(HASH_PATH):
        while(True):
            print('Secret key already set: delete it (y/n)')
            choice = raw_input().lower()
            if choice == 'y':
                remove(HASH_PATH)
                break
            elif choice == 'n':
                exit()

    # get current user
    # user = getuser()

    upass = getpass()
    vupass = getpass('Verify password: ')
    if upass != vupass:
        print('Error: passwords do not match')
        exit(1)

    sk = SecretKey()
    sk.generate_key(password=upass)

    print('Secret key created')


if __name__ == '__main__':
    main()