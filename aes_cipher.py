from base64 import b64decode
from base64 import b64encode
from Crypto import Random
from Crypto.Cipher import AES


import base64
from Crypto.PublicKey import RSA

KEY_PATH = '.key'


class AESCipher:
    """
    Usage:
        encrypt_msg = aes.encrypt_to_file(
                                source=RAW,
                                key=key, 
                                fname=KEY_PATH)
`
        data = aes.decrypt_from_file(
                                fname=KEY_PATH,
                                key=key)
    """

    def encrypt_to_file(self, source, key, fname=None):
        """
        encrypt source data and write file
        Args:
            source: str, data for encrypt
            key: str, secret key
            fname: str: file name for write encrypted data
        Raises:
            OSError: failed to write encrypt data to file 
        Return:
            encrypted data
        """
        if not fname:
            fname = KEY_PATH

        encrypt_msg = self.encrypt(key=key, source=source)

        try:
            with open(fname, 'wb') as f:
                f.write(encrypt_msg)
            return encrypt_msg
        except OSError as os_err:
            raise OSError(
                'failed to write encrypt data to %s: %s' 
                % (fname, os_err))

    def encrypt(self, key, source):
        """
        encrypt source data with phrase
        Args:
           source: str, source data
           key: secret key
        Raises:
        Returns:
            encrypted text
        """
        IV = Random.new().read(AES.block_size)
        encryptor = AES.new(key, AES.MODE_CBC, IV)

        # calculate needed padding
        padding = AES.block_size - len(source) % AES.block_size
        source += chr(padding) * padding

        # store the IV at the beginning and encrypt
        data = IV + encryptor.encrypt(source)  
        return base64.b64encode(data).decode("latin-1")

    def decrypt_from_file(self, key, fname=None):
        """
        open file read encrypted data and decrypt with secret key
        Args:
            fname: str, file name with encrypted data
            key: str, secret key
        Raises:
            OSError: failed to open file with 
        Returns:
            raw text after decrypt
        """
        try:
            if not fname:
                fname = KEY_PATH

            encrypted_msg = open(fname, 'r').read()
            return self.decrypt(source=encrypted_msg, key=key)
        except OSError as os_err:
            raise os_err
        except Exception as e:
            raise e

    def decrypt(self, key, source):
        source = base64.b64decode(source.encode("latin-1"))
        
        # extract the IV from the beginning
        IV = source[:AES.block_size]
        decryptor = AES.new(key, AES.MODE_CBC, IV)
        data = decryptor.decrypt(source[AES.block_size:])

        # pick the padding value from the end
        padding = ord(data[-1])                            
        if data[-padding:] != chr(padding) * padding:
            raise ValueError("Invalid padding...")
        
        # remove the padding
        return data[:-padding]