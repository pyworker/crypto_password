#!/usr/bin/env python2.7

"""
module for creating a user account
"""


from getpass import getpass, getuser
from sys import stdout, exit
from os.path import exists
from os import chmod, remove
from stat import S_IRUSR
from aes_cipher import AESCipher
from secret_key import SecretKey


HASH_PATH = '.hash'
AUTH_PATH = '.auth'


def main():
    if not exists(HASH_PATH):
        print('Error: need to create secret key: run "python create_key"')
        exit(1)

    if exists(AUTH_PATH):
        while(True):
            stdout.write('Auth already set: delete it (y/n)')
            choice = raw_input().lower()
            if choice == 'y':
                remove(AUTH_PATH)
                break
            elif choice == 'n':
                exit()


    stdout.write('Login: ')

    # enter login 
    login = raw_input()

    # enter password
    upass = getpass()
    vupass = getpass('Verify password: ')
    if upass != vupass:
        print('Error: passwords do not match')
        exit(1)

    aes = AESCipher()
    sk = SecretKey()
    key = sk.get()

    try:
        with open(AUTH_PATH, 'wb') as f:
            f.write(':'.join([login, aes.encrypt(source=upass, key=key)]))
        chmod(AUTH_PATH, S_IRUSR)
    except Exception as e:
        raise e


if __name__ == '__main__':
    main()