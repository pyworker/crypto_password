from hashlib import md5


from Crypto.PublicKey import RSA

HASH_PATH = '.hash'

class SecretKey(object):
    """docstring for SecretKey"""
    def __init__(self):
        pass

    def generate_key(self, password, fname=HASH_PATH):
        """
        generate new key and write it to file with fname
        Args:
            password: str, raw key
            fname: str, filename for key
        Raises:
        Retirn:
        """
        key = md5(password.encode('utf8')).hexdigest()
        try:
            with open(fname, 'wb') as f:
                f.write(key)
            return key
        except OSError as os_err:
            raise OSError(
                'failed to write file with name %s: %s'
                % (fname, os_err))

    def get(self, fname=None):
        """
        return key
        Args:
            fname: str, filename with key
        Returns;
            secret key
        """
        if not fname:
            fname = HASH_PATH
        return open(fname, 'r').read()

