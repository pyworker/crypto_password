from os.path import exists


HASH_PATH = '.hash'
AUTH_PATH = '.auth'


def main():
    if not exists(AUTH_PATH):
        print('Error: need to set auth: run "python create_auth"')
        exit(1)

    with open(AUTH_PATH, 'r') as f:
        auth = f.read()

    if not auth:
        print('failed to get auth')

    auth = auth.split(':', 1)
    login = auth[0]
    password = auth[1]
    print(login)
    print(password)


if __name__ == '__main__':
    main()