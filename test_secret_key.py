import unittest
from os import remove
from os.path import exists

from secret_key import SecretKey
from hashlib import md5

TEST_KEY_PATH = '.test_key'
TEST_HASH_PATH = '.test_hash'
RAW = 'secret_phrase'


class TestSecretKey(unittest.TestCase):    
    def test_generate_key_algorithm(self):
        """
        currect algorithm work
        """
        k = SecretKey()
        # prepare env before test 
        for item in [TEST_KEY_PATH, TEST_HASH_PATH]:
            try:
                remove(item)
                print('removed %s' % item)
            except OSError:
                pass

        ans = k.generate_key(password=RAW, fname=TEST_HASH_PATH)
        should_ans = md5(RAW.encode('utf8')).hexdigest()
        self.assertEqual(ans, should_ans)

        # clean env after test 
        for item in [TEST_KEY_PATH, TEST_HASH_PATH]:
            try:
                remove(item)
                print('removed %s' % item)
            except OSError:
                pass

    def test_generate_key_create_hash_file(self):
        """
        currect create file
        """
        k = SecretKey()
        # prepare env before test 
        for item in [TEST_KEY_PATH, TEST_HASH_PATH]:
            try:
                remove(item)
                print('removed %s' % item)
            except OSError:
                pass

        k.generate_key(password=RAW, fname=TEST_HASH_PATH)
        ans = exists(TEST_HASH_PATH)
        should_ans = True
        self.assertEqual(ans, should_ans)

        # clean env after test 
        for item in [TEST_KEY_PATH, TEST_HASH_PATH]:
            try:
                remove(item)
                print('removed %s' % item)
            except OSError:
                pass

        self.assertEqual(ans, should_ans)        


if __name__ == '__main__':
    unittest.main()
